#!/bin/bash

ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
hwclock --systohc
sed -i '394s/.//' /etc/locale.gen
locale-gen
echo "LANG=pt_BR.UTF-8" >>/etc/locale.conf
echo "KEYMAP=br-abnt2" >>/etc/vconsole.conf
echo "arch" >>/etc/hostname
echo "127.0.0.1 localhost" >>/etc/hosts
echo "::1       localhost" >>/etc/hosts
echo "127.0.1.1 arch.localdomain arch" >>/etc/hosts
echo root:password | chpasswd

# You can add xorg to the installation packages, I usually add it at the DE or WM install script

pacman -S --noconfirm grub networkmanager network-manager-applet dialog mtools base-devel linux-headers xdg-user-dirs xdg-utils nfs-utils inetutils dnsutils pulseaudio bash-completion openssh rsync reflector iptables-nft ipset firewalld flatpak

# pacman -S --noconfirm xf86-video-amdgpu
# pacman -S --noconfirm nvidia nvidia-utils nvidia-settings

grub-install --target=i386-pc /dev/sdX # replace sdx with your disk name, not the partition
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
systemctl enable sshd
systemctl enable avahi-daemon
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable firewalld

useradd -m frost
echo frost:password | chpasswd

echo "frost ALL=(ALL) ALL" >>/etc/sudoers.d/frost

printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"
