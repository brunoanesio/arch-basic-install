#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc

sudo reflector -c Brazil -a 6 --sort rate --save /etc/pacman.d/mirrorlist

sudo firewall-cmd --add-port=1025-65535/tcp --permanent
sudo firewall-cmd --add-port=1025-65535/udp --permanent
sudo firewall-cmd --reload

# git clone https://aur.archlinux.org/paru.git
# cd paru/
# makepkg -si --noconfirm

# paru -S --noconfirm system76-power
# sudo systemctl enable --now system76-power
# sudo system76-power graphics integrated
# paru -S --noconfirm auto-cpufreq
# sudo systemctl enable --now auto-cpufreq

sudo pacman -S --noconfirm xorg sddm plasma-meta konsole firefox mpv noto-fonts noto-fonts-emoji noto-fonts-cjk ffmpeg ffmpegthumbs kdegraphics-thumbnailers plasma-firewall

sudo systemctl enable sddm
/bin/echo -e "\e[1;32mREBOOTING IN 5..4..3..2..1..\e[0m"
sleep 5
reboot
